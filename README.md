# Bundling With HTTP/2
The talk I gave at JSConf.Asia 2016 to publicly announce [http2server](https://www.npmjs.com/package/http2server) and [unbundle](https://www.npmjs.com/package/unbundle).

## Slides

[![Play button](http://icon-park.com/imagefiles/movie_play_blue.png)](https://sebdeckers.gitlab.io/talk-http2server-unbundle/)

- [HTML live preview](https://sebdeckers.gitlab.io/talk-http2server-unbundle/)
- [PDF](https://gitlab.com/sebdeckers/talk-http2server-unbundle/raw/master/Bundling%20With%20HTTP:2%20-%20JSConf.Asia%202016.pdf)
- [Keynote (source)](https://gitlab.com/sebdeckers/talk-http2server-unbundle/raw/master/Bundling%20With%20HTTP:2%20-%20JSConf.Asia%202016.key)
